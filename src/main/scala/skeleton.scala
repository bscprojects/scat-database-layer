/** Ez egy tesz alkalmazas a SCAT adatbazis feluletehez.
	*
	* A program letrehozza a tablakat a memoriaban, es ezekhez elero- es modosito
	* fuggvenyeket biztosit.
	* A tablakban egyelore nincsenek datumos mezok, ennek felvetele kesobb tortenik,
	* de a metodusok nevei veglegesnek tekinthetok, a tovabbi egyeztetesig.
	*/


import scala.slick.driver.H2Driver.simple._
import Database.threadLocalSession

object DatabaseExample {
	
	/** EXCEPTIONS */
	
	object UserAlreadyExists extends Exception { }
	object UserNotExists extends Exception { }
	object ChatNotExists extends Exception { }
	
	
	
	
	/** TABLE DEFINITIONS */
	
	case class User(id: Int, login: String, password: String, nick: String, email: String)
	case class Chat(id: Int, creatorID: Int, title: String, url: String)
	case class Message(id: Int, chatID: Int, senderID: Int, msg: String)
	
	/** Definition of USERS table */
		object Users extends Table[User]("USERS") {
			
			//attributes
			def id = column[Int]("USER_ID", O.PrimaryKey, O.AutoInc)
			def login = column[String]("LOGIN")
			def password = column[String]("PASSWORD")
			def nick = column[String]("NICK")
			def email = column[String]("EMAIL")
			
			def * = id ~ login ~ password ~ nick ~ email <> (User, User.unapply _)
			def ins = login ~ password ~ nick ~ email returning id
			
			def byID = createFinderBy(_.id)
			def byNick = createFinderBy(_.nick)
			def byLogin = createFinderBy(_.login)
		}
		
		/** Definition of CHATS table */
		object Chats extends Table[Chat]("CHATS") {
		
			//attributes
			def id = column[Int]("CHAT_ID", O.PrimaryKey, O.AutoInc)
			def creatorID = column[Int]("CREATOR_ID")
			def title = column[String]("TITLE")
			def url = column[String]("URL")
			
			def * = id ~ creatorID ~ title ~ url <> (Chat, Chat.unapply _)
			def ins = creatorID ~ title ~ url returning id
			def byID = createFinderBy(_.id)
			
			def creatorFK = foreignKey("CREATOR_FK", creatorID, Users)(_.id)
		}
		
		/** Definition of MESSAGES table */
		object Messages extends Table[Message]("MESSAGES") {
		
			//attributes
			def id = column[Int]("MSG_ID", O.PrimaryKey, O.AutoInc)
			def chatID = column[Int]("CHAT_ID")
			def senderID = column[Int]("SENDER_ID")
			def msg = column[String]("MESSAGE")
			
			def * = id ~ chatID ~ senderID ~ msg <> (Message, Message.unapply _)
			def ins = chatID ~ senderID  ~ msg returning id
			
			def chatFK = foreignKey("CHAT_FK", chatID, Chats)(_.id)
			def senderFK = foreignKey("SENDER_ID", senderID, Users)(_.id)
			def byID = createFinderBy(_.id)
		}
	
	
	
	
	
	
	/** TABLE OPERATIONS */
	
	/** Operations with users */
	
			/** Select User by given identifier */
			def getUserByID(userID: Int): Option[User] = {
				Users.byID(userID).firstOption
			}
			
			/** Select User by given login */
			def getUserByLogin(login: String): Option[User] = {
				Users.byLogin(login).firstOption
			}
			
			/** Select User by given nickname */
			def getUserByNick(nick: String): Option[User] = {
				Users.byNick(nick).firstOption
			}
			
			/** Returns a list with all User objects */
			def getAllUsers(): List[User] = {
				(for ( u <- Users) yield u).list
			}
			
			/** Tests whether a User is in the Users table ~ Option[User] is not None */
			def isExistingUser(u: Option[User]): Boolean = {
				if (u == None) false
				else true
			}
			
			/** Add new User to Users table, if the given login is not already taken */
			def addUser(login: String, passw: String, nick: String, email: String): Unit = {
				if(isExistingUser(getUserByLogin(login))) {
					throw UserAlreadyExists
				}
				else {
					Users.ins.insert(login, passw, nick, email)
				}
			}
			
			/** Changes login's nickname to newnick */
			def modifyUserNick(login: String, newnick: String): Unit = {
				if(isExistingUser(getUserByLogin(login))) {
						//modify nickname in Users
						val n = Users.byLogin(login).first.copy(nick = newnick)
						
						Query(Users).where(_.login === login).update(n)
					}
				else {
					throw UserNotExists
				}
			}
	
	
	
	/** Operations with chat entities */
	
			/** Insert new Chat to Chats table through given user login as creator */
			def addChat(creatorLogin: String, title: String, url: String): Unit = {
				val u = getUserByLogin(creatorLogin)
				
				if(!isExistingUser(u)) {
					throw UserNotExists
				}
				else {
					val creatorID = u.get.id
					Chats.ins.insert(creatorID, title, url)
				}
			}
			
			/** Insert new Chat to Chats table with userID as creator */
			def addChat(creatorID: Int, title: String, url: String): Unit = {
				val u = getUserByID(creatorID)
				
				if(!isExistingUser(u)) {
					throw UserNotExists
				}
				else {
					val creatorID = u.get.id
					Chats.ins.insert(creatorID, title, url)
				}
			}
			
			/** Return with a Chat object if the chatID is existing */
			def getChatByID(chatID: Int): Option[Chat] = {
				Chats.byID(chatID).firstOption
			}
			
			/** Test whether a chat is existing ~ Option[Chat] not None */
			def isExistingChat(c: Option[Chat]): Boolean = {
				if (c == None) false
				else true
			}
			
			/** Changes the Chat's title to new title */
			def modifyChatTitle(chatID: Int, newtitle: String): Unit = {
				val c = getChatByID(chatID)
				
				if(!isExistingChat(c)) {
					throw ChatNotExists
				}
				else {
					val n = c.get.copy(title = newtitle)
					Query(Chats).where(_.id === chatID).update(n)
				}
			}

	
	
	/** Operations with messages */
	
	/** Insert new Message to Messages table - sender is given by login*/
	def addMessage(chatID: Int, senderLogin: String, msg: String): Unit = {
		val u = getUserByLogin(senderLogin)
		
		if(!isExistingUser(u)) {
			throw UserNotExists
		}
		else if (!isExistingChat(getChatByID(chatID))) {
			throw ChatNotExists
		}
		else {
			Messages.ins.insert(chatID, u.get.id, msg)
		}
	}
	
	/** Insert new Message to Messages table - sender is given by id*/
	def addMessage(chatID: Int, senderID: Int, msg: String): Unit = {
		val u = getUserByID(senderID)
		
		if(!isExistingUser(u)) {
			throw UserNotExists
		}
		else if (!isExistingChat(getChatByID(chatID))) {
			throw ChatNotExists
		}
		else {
			Messages.ins.insert(chatID, senderID, msg)
		}
	}
	
	
	
	/** Operations via foreig key connections */
	
	/** Whick Users are is the given Chat? */
	def getUsersInChat(chatID: Int): List[(Int, String)] = {
		if (!isExistingChat(getChatByID(chatID))) throw ChatNotExists
		
		val usersInChat = for { 
				m <- Messages if m.chatID === chatID
				u <- m.senderFK
			} yield (u.id, u.nick)
			
		usersInChat.list.distinct
	}
	
	/** Return a list with all messages is the given chat, in (sebderNick, message) format */
	def getMessagesInChat(chatID: Int): List[(String, String)] = {
		if (!isExistingChat(getChatByID(chatID))) throw ChatNotExists
		
		val messagesInChat = for {
			m <- Messages if m.chatID === chatID
			u <- m.senderFK
		} yield (u.nick, m.msg)
		
		messagesInChat.list
	}
	
	
	
	def main(args: Array[String]) {
		
		//Database connection
		Database.forURL("jdbc:h2:mem:test1", driver = "org.h2.Driver") withSession {
			
			//Create tables and connections
			(Users.ddl ++ Chats.ddl ++ Messages.ddl).create
			
			//Insert some content
			
					Users.ins.insert("login1", "pwd1", "nick_1", "user1@scat.com")
					Users.ins.insert("login2", "pwd2", "nick_2", "user2@scat.com")
					Users.ins.insert("login3", "pwd3", "nick_3", "user3@scat.com")
					Users.ins.insert("login4", "pwd4", "nick_4", "user4@scat.com")
					Users.ins.insert("login5", "pwd5", "nick_5", "user5@scat.com")
					
					Chats.ins.insertAll(
						(1, "ChatTitle1", "scat.com/chat1"),
						(2, "ChatTitle2", "scat.com/chat2"),
						(1, "ChatTitle3", "scat.com/chat3")
					);
					
					Messages.ins.insertAll(
						(1, 1, "asdf1"), (1, 1, "asdf2"), (1, 2, "asdf3"), (1, 1, "asdf4"),
						(2, 2, "asdf5"), (2, 4, "asdf6"), (2, 3, "asdf7"),
						(3, 1, "asdf6"), (3, 2, "asdf7"),  (3, 3, "asdf8"), (3, 4, "asdf9"), (3, 5, "asdf10")
					);
			
			//Queries
			
					println("Users table:")
					Query(Users) foreach { case u =>
						println(" " + u.id + "\t" + u.login + "\t" + u.password + "\t" + u.nick + "\t" + u.email) }
					println
					
					println("Chats table:")
					Query(Chats) foreach { case c =>
						println(" " + c.id + "\t" + c.creatorID + "\t" + c.title + "\t" + c.url) }
					println
					
					println("Messages table:")
					Query(Messages) foreach { case m =>
						println(" " + m.id + "\t" + m.chatID + "\t" + m.senderID + "\t" + m.msg) }
					println
			
			//functions about Users
			
					addUser("login6", "pwd6", "nick_66", "user6@scat.com")
					//addUser("login3", "pwd", "nick", "email") - UserAlreadyExists exception
					
					modifyUserNick("login2", "nick_22")
					//modifyUserNick("login20", "nick_222") - UserNotExists exception
					
					println("Users table after modification:")
					Query(Users) foreach { case u =>
						println(" " + u.id + "\t" + u.login + "\t" + u.password + "\t" + u.nick + "\t" + u.email) }
					println
			
			//functions about Chats
			
					addChat("login3", "chattitle", "scat.com/chatn")
					//addChat("login666", "chattitle", "scat") -  UserNotExists exception
					
					addChat(6, "chattitle", "scat.com/chatn+1")
					//addChat(1000, "chattitle", "scat") -  UserNotExists exception
					
					println("Chats table after modification:")
					Query(Chats) foreach { case c =>
						println(" " + c.id + "\t" + c.creatorID + "\t" + c.title + "\t" + c.url) }
					println
			
			//functions about Messages
			
					addMessage(2, "login6", "msg")
					//addMessage(2, "login666", "msg") -  UserNotExists exception
					//addMessage(8, "login6", "msg") -  ChatNotExists exception
					
					addMessage(3, 6, "msg")
					//addMessage(3, 666, "msg") -  UserNotExists exception
					//addMessage(10, 6, "msg") -  ChatNotExists exception
					
					println("Messages table after modification:")
					Query(Messages) foreach { case m =>
						println(" " + m.id + "\t" + m.chatID + "\t" + m.senderID + "\t" + m.msg) }
					println
					
			//functions via foreign key connections
			
					println("Users in chat 1:")
					getUsersInChat(1) foreach println
					println
					
					println("Messages in chat 3:")
					getMessagesInChat(3) foreach println
					println
		}
	}
}