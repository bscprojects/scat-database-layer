import org.scalatest._
import org.scalatest.matchers.ShouldMatchers

import DatabaseExample._

class Hi extends FunSpec 
	with ShouldMatchers
	with BeforeAndAfter
	with BeforeAndAfterEach
	with GivenWhenThen
{
	describe ("User table of the database")
	{
		it("can add user to the database, and we can get all of them")
		{

			addUser("login","passw","nick","email@email.email")
			addUser("login2","passw2","nick2","email2@email.email")
			addUser("login3","passw3","nick3","email3@email.email")

			val users = getAllUsers();

			users should have size 3

			users(0) should have(
				'login ("login"),
				'password("passw"),
				'nick ("nick"),
				'email ("email@email.email")
			)
			users(1) should have(
				'login ("login2"),
				'password("passw2"),
				'nick ("nick2"),
				'email ("email2@email.email")
			)
			users(2) should have(
				'login ("login3"),
				'password("passw3"),
				'nick ("nick3"),
				'email ("email3@email.email")
			)
		}
		it ("can give us a single user")
		{
			addUser("cguasulogin","cguasupassw","cguasunick","cguasuemail@email.email")
			getUserByLogin("cguasulogin") should be ('defined)
			getUserByNick("cguasunick") should be ('defined)
			val id1 = getUserByLogin("cguasulogin").get.id
			val id2 = getUserByNick("cguasunick").get.id
			id1 should equal (id2)
			getUserByID(id1) should be ('defined)
		}
		it ("gives None if we request a user that can't be found")
		{
			getUserByLogin("thereisnologinlikethis") should be (None)
			getUserByNick("thereisnonicknamelikethis") should be (None)
		}

		it ("can tell us whether a requested user exists or not")
		{
			addUser("existlogin","existpassw","existnick","existemail@email.email")
			isExistingUser(getUserByLogin("existlogin")) should equal (true)
			isExistingUser(getUserByLogin("thereisnologinlikethis")) should equal (false)
		}

		it ("can modify a existing user's nick")
		{
			addUser("modifylogin","modifypassw","modifynick","modifyemail@email.email")
			modifyUserNick("modifylogin", "ujnick")
			getUserByLogin("modifylogin").get.nick should equal ("ujnick")
		}



	}

	describe ("The chat table of the database")
	{
		it ("""can add new chat to the table, and we can get it
			it is neccessary that the creator is an existing user id or login""")
		{

			addUser("creatorlogin","creatorpassw","creatornick","creatoremail@email.email")
			addChat("creatorlogin", "title", "url://url.url")

			(pending)
		}
	}
	
	describe("The messages table of the database")
	{
	  
	}
}

